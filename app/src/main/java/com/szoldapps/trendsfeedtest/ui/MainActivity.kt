package com.szoldapps.trendsfeedtest.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.szoldapps.trendsfeedtest.R
import com.szoldapps.trendsfeedtest.service.EXTRA_ERROR
import com.szoldapps.trendsfeedtest.service.EXTRA_TRENDS
import com.szoldapps.trendsfeedtest.service.RESPONSE_LOAD_TRENDS
import com.szoldapps.trendsfeedtest.service.TwitterIntentService
import com.szoldapps.trendsfeedtest.service.model.Trend
import kotlinx.android.synthetic.main.activity_main.pbLoading
import kotlinx.android.synthetic.main.activity_main.rvTrends


class MainActivity : AppCompatActivity() {

    private lateinit var viewAdapter: TrendsAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = getString(R.string.action_bar_title)

        viewManager = LinearLayoutManager(this)
        viewAdapter = TrendsAdapter(mutableListOf())

        rvTrends.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
            addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
        }
    }

    private fun loadTrends() {
        // load trends (via IS)
        TwitterIntentService.loadTrends(applicationContext)
        showLoading(true)
    }

    override fun onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter(RESPONSE_LOAD_TRENDS))
        // load trends (via IS)
        loadTrends()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    private val broadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val isError = intent.getBooleanExtra(EXTRA_ERROR, false)
            if (isError) {
                Toast.makeText(applicationContext, getString(R.string.error_toast_msg), Toast.LENGTH_LONG).show()
                // hide all
                pbLoading.visibility = View.GONE
                rvTrends.visibility = View.GONE
            } else {
                showTrends(intent.getParcelableArrayListExtra<Trend>(EXTRA_TRENDS))
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refresh -> {
                loadTrends()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showTrends(trends: List<Trend>) {
        viewAdapter.refresh(trends)
        showLoading(false)
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            // show loading
            pbLoading.visibility = View.VISIBLE
            // hide recyclerView
            rvTrends.visibility = View.GONE
        } else {
            // hide loading
            pbLoading.visibility = View.GONE
            // show recyclerView
            rvTrends.visibility = View.VISIBLE
        }

    }

}

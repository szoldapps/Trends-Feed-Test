package com.szoldapps.trendsfeedtest.ui

import android.content.Intent
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.szoldapps.trendsfeedtest.R
import com.szoldapps.trendsfeedtest.service.model.Trend


class TrendsAdapter(private val trends: MutableList<Trend>) : RecyclerView.Adapter<TrendsAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val clTrend: ConstraintLayout = v.findViewById(R.id.clTrend)
        val tvName: TextView = v.findViewById(R.id.tvName)
        val tvTweetVolume: TextView = v.findViewById(R.id.tvTweetVolume)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_trend, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val trend = trends[position]
        holder.tvName.text = trend.name
        holder.tvTweetVolume.text = if (trend.tweetVolume != -1) trend.tweetVolume.toString() else ""
        holder.clTrend.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(trend.url)
            startActivity(holder.tvName.context, intent, null)
        }

    }

    override fun getItemCount() = trends.size

    fun refresh(newTrends: List<Trend>) {
        trends.clear()
        trends.addAll(newTrends)
        notifyDataSetChanged()
    }

}
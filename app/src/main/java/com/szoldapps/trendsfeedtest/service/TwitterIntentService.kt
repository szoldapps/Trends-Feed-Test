package com.szoldapps.trendsfeedtest.service

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Base64
import android.util.Log
import com.szoldapps.trendsfeedtest.BuildConfig
import com.szoldapps.trendsfeedtest.service.model.Trend
import com.szoldapps.trendsfeedtest.service.model.mapToTrend
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.FileNotFoundException
import java.net.URL
import javax.net.ssl.HttpsURLConnection


private const val ACTION_LOAD_TRENDS = "com.szoldapps.trendsfeedtest.action.LOAD_TRENDS"
const val RESPONSE_LOAD_TRENDS = "com.szoldapps.trendsfeedtest.response.LOAD_TRENDS"
const val EXTRA_TRENDS = "com.szoldapps.trendsfeedtest.extra.LOAD_TRENDS"
const val EXTRA_ERROR = "com.szoldapps.trendsfeedtest.extra.ERROR"

class TwitterIntentService : IntentService("TwitterIntentService") {

    private val tag = javaClass.simpleName

    companion object {

        fun loadTrends(context: Context) {
            context.startService(
                Intent(context, TwitterIntentService::class.java).apply {
                    action = ACTION_LOAD_TRENDS
                }
            )
        }

    }

    override fun onHandleIntent(intent: Intent?) {
        when (intent?.action) {
            ACTION_LOAD_TRENDS -> loadTrends()
        }
    }

    private fun loadTrends() {
        // WOEID 638242 = Berlin, Germany
        val woeId = 638242
        val connection = URL("https://api.twitter.com/1.1/trends/place.json?id=$woeId").openConnection() as HttpsURLConnection
        try {
            // set connection properties
            with(connection) {
                requestMethod = "GET"
                setRequestProperty("Authorization", "Bearer ${accessToken()}")
                setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
            }
            // get data
            val jsonString = connection.inputStream.bufferedReader().readText()
            // parse JSON
            sendMessage(extractTrendsByParsing(jsonString))
        } catch (e: FileNotFoundException) {
            sendError()
        } finally {
            connection.disconnect()
        }

    }

    private fun extractTrendsByParsing(jsonString: String): ArrayList<Trend> {
        val containerArray = JSONArray(jsonString)
        val containerObject = containerArray.getJSONObject(0)
        val trendsArray = containerObject.getJSONArray("trends")
        val trends = ArrayList<Trend>()
        for (i in 0 until trendsArray.length()) {
            val trend = trendsArray.getJSONObject(i).mapToTrend()
            Log.d(tag, "name($i) = ${trend.name}")
            trends.add(trend)
        }
        return trends
    }

    private fun accessToken(): String {
        val connection = URL("https://api.twitter.com/oauth2/token").openConnection() as HttpsURLConnection
        try {
            // set connection properties
            with(connection) {
                requestMethod = "POST"
                setRequestProperty("Authorization", "Basic ${encodedConsumerKeyAndSecret()}")
                setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
            }
            // set body
            val body = "grant_type=client_credentials".toByteArray(charset("UTF-8"))
            with(connection.outputStream) {
                write(body)
                close()
            }
            // get data
            val jsonData = connection.inputStream.bufferedReader().readText()
            val jsonObj = JSONObject(jsonData)
            val token = jsonObj.getString("access_token")

            Log.d(tag, "token = $token")
            return token
        } catch (jsonException: JSONException) {
            return ""
        } finally {
            connection.disconnect()
        }
    }

    private fun encodedConsumerKeyAndSecret(): String {
        val consumerKey = BuildConfig.TWITTER_CONSUMER_KEY
        val consumerSecret = BuildConfig.TWITTER_CONSUMER_SECRET
        val byteArrayConsumerKeyAndSecret = "$consumerKey:$consumerSecret".toByteArray(charset("UTF-8"))
        return Base64.encodeToString(byteArrayConsumerKeyAndSecret, Base64.NO_WRAP)
    }

    private fun sendMessage(trends: ArrayList<Trend>) {
        LocalBroadcastManager.getInstance(this)
            .sendBroadcast(
                Intent(RESPONSE_LOAD_TRENDS).putParcelableArrayListExtra(EXTRA_TRENDS, trends)
            )
    }

    private fun sendError() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(
            Intent(RESPONSE_LOAD_TRENDS).putExtra(EXTRA_ERROR, true)
        )
    }

}

package com.szoldapps.trendsfeedtest.service.model

import android.os.Parcel
import android.os.Parcelable


data class Trend(
    val name: String = "",
    val url: String = "",
    val promotedContent: String = "",
    val query: String = "",
    val tweetVolume: Int = -1
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
        dest.writeString(url)
        dest.writeString(promotedContent)
        dest.writeString(query)
        dest.writeInt(tweetVolume)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Trend> {
        override fun createFromParcel(parcel: Parcel): Trend {
            return Trend(parcel)
        }

        override fun newArray(size: Int): Array<Trend?> {
            return arrayOfNulls(size)
        }
    }
}

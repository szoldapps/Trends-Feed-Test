package com.szoldapps.trendsfeedtest.service.model

import org.json.JSONException
import org.json.JSONObject


fun JSONObject.mapToTrend(): Trend =
    Trend(
        name = try {
            getString("name")
        } catch (e: JSONException) {
            ""
        },
        url = try {
            getString("url")
        } catch (e: JSONException) {
            ""
        },
        promotedContent = try {
            getString("promoted_content")
        } catch (e: JSONException) {
            ""
        },
        query = try {
            getString("query")
        } catch (e: JSONException) {
            ""
        },
        tweetVolume = try {
            getInt("tweet_volume")
        } catch (e: JSONException) {
            -1
        }
    )

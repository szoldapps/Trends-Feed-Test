# **iOS/Android/React Native Mobile Developer Coding Task**
Many thanks for investing the time in interviewing with us. For the next step we would like you to create a Twitter mobile client (iOS/Android/React Native app) to display the 50 most trending topics around a users' current location.

Please submit the code for the **specific platform** your were **asked to in the interview**. 

## **Please follow the following guidelines while developing : -**
* Please use the [Twitter Trends API](https://developer.twitter.com/en/docs/trends/trends-for-location/api-reference/get-trends-place) to fetch the relevant trends data.
* Please ***refrain*** from using any additional 3rd party APIs (*e.g. Alamofire, Retrofit, RxJava etc...*) except for the iOS or Google Android SDK, as required.
* Please focus on proper code structure and applying OOP and/or SOLID coding principles as and when applicable.
* Please don't spend *too much time* on producing a snazzy UI; we are more interested in your **coding competency**.
* Ideally you shouldn't spend more than *4 hours* to finish this task.
* Please ensure you submit the task within **7 working days** from receipt.

Please don't hesitate to contact us in case of any further queries.

[original assignment link](https://gist.github.com/aprofromindia/52537ec8084b2e00ca990549a3613d40)